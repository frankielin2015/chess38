package com.group38.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.group38.model.PlayedGames;

public class GameListActivity extends AppCompatActivity implements OnItemClickListener {

	private ListView listView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_list_activity);
		
		final ListView v = (ListView)findViewById(R.id.listView);
		
		 ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                 android.R.layout.simple_list_item_1, android.R.id.text1,
                 PlayedGames.gameNames.toArray(new String[PlayedGames.gameNames.size()]));
		 
		 v.setAdapter(adapter);
		 v.setOnItemClickListener(this);
		 listView = v;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.game_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		
		PlayedGames.activeIndex = position;
		startActivity(new Intent(GameListActivity.this, ReplayActivity.class));
		
	}
	
}
