package com.group38.activities;

import java.util.LinkedList;
import java.util.ListIterator;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.GridView;

import com.group38.controller.Game;
import com.group38.model.Move;
import com.group38.model.PlayedGames;
import com.group38.adapter.SquareAdapter;

public class ReplayActivity extends Activity {

	private Game game;
	private static boolean RUN_ONCE = false;
	private SquareAdapter adapter;
	private GridView chessboard;
	private LinkedList<Move> moves;
	private ListIterator<Move> listIterator;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.replay_activity);
		
		if (!RUN_ONCE) {

			RUN_ONCE = true;
			this.game = new Game();
			moves = PlayedGames.playedGames.get(PlayedGames.activeIndex);
			listIterator = moves.listIterator();
			adapter = new SquareAdapter(this, game.getBoard());

		}

		final GridView chessBoardGridView = (GridView)findViewById(R.id.chessboard);

		initNextButton();
		initPreviousButton();
		chessBoardGridView.setAdapter(adapter);


		this.chessboard = chessBoardGridView;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.replay, menu);
		return true;
	}


	private void initNextButton() {

		Button nextButton = (Button) findViewById(R.id.nextButton);
		nextButton.setOnClickListener(new OnClickListener() {

			@Override 
			public void onClick(View argo) {

				if (!listIterator.hasNext()) return;
				
				Move move = listIterator.next();
				game.move(move.getSourcePosition(), move.getDestPosition());
				adapter.notifyDataSetChanged();
				chessboard.setAdapter(adapter);
			}
		});
	}
	
	private void initPreviousButton() {

		Button previousButton = (Button) findViewById(R.id.previousButton);
		previousButton.setOnClickListener(new OnClickListener() {

			@Override 
			public void onClick(View argo) {

				if (!listIterator.hasPrevious()) return;
				listIterator.previous();
				game.undo();
				adapter.notifyDataSetChanged();
				chessboard.setAdapter(adapter);
			}
		});
	}


	@Override
	public void onBackPressed() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Exit");
		builder.setMessage("Quit game?");

		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				startActivity(new Intent(ReplayActivity.this, HomeActivity.class));
				RUN_ONCE = false;
				finish();
			}
		});

		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case (R.id.action_settings):
			return true;
		case (android.R.id.home):
			onBackPressed();
		return true;
		}

		return super.onOptionsItemSelected(item);
	}
}
