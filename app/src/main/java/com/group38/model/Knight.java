package com.group38.model;

public class Knight extends ChessPiece {

	
	public String getPieceName() { return "knight"; }

	
	public boolean isValidMove(Square dest) 
	{
		int xPos = Math.abs( dest.getX() - getLocation().getX());
		int yPos= Math.abs( dest.getY() - getLocation().getY());
		
		if ((xPos == 2 && yPos == 1)) return true;
		
		if ((xPos == 1 && yPos == 2)) return true;


		return false;
	}

}
