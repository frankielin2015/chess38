package com.group38.model;

import java.io.Serializable;


public class Square implements Serializable{

	
	private int x;
	private int y;
	
	private static final long serialVersionUID = 1L;
	
	private ChessPiece piece;
	
	public int getX() { return x; }
	
	public int getY() { return y; }
	
	public Square(int r, int c) {
		this.y = r;
		this.x = c;
	}
	
	public ChessPiece getPiece() { return piece; }
	
	public void setPiece(ChessPiece piece) { this.piece = piece; }
	
	@Override
	public boolean equals(Object o) {
		
		if (!(o instanceof Square)) return false;
		
		if (this.getX() == ((Square) o).getX() && this.getY() == ((Square) o).getY()) return true;
		
		return false;
	}
}
