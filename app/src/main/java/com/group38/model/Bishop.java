package com.group38.model;

public class Bishop extends ChessPiece {
	


	
	public String getPieceName() { return "bishop"; }

	
	public boolean isValidMove(Square dest) {
		int xPos = Math.abs( dest.getX() - getLocation().getX());
		int yPos= Math.abs( dest.getY() - getLocation().getY());
		if(xPos != yPos)
		{
			return false;
		}
		return this.clearPathTo(dest);
	}
	
}
