package com.group38.model;

public class Rook extends ChessPiece {

	public String getPieceName() { return "rook"; }

	public boolean isValidMove(Square dest) {


		if (getLocation().getY() != dest.getY() && getLocation().getX() != dest.getX()) return false;
		
		return this.clearPathTo(dest);
	}

}
